=============================
ZS Core Utils
=============================

.. image:: https://badge.fury.io/py/zs-core-utils.png
    :target: https://badge.fury.io/py/zs-core-utils

.. image:: https://travis-ci.org/aidzz/zs-core-utils.png?branch=master
    :target: https://travis-ci.org/aidzz/zs-core-utils

Your project description goes here

Documentation
-------------

The full documentation is at https://zs-core-utils.readthedocs.org.

Quickstart
----------

Install ZS Core Utils::

    pip install zs-core-utils

Then use it in a project::

    import zs_core

Features
--------

* TODO

Running Tests
--------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install -r requirements_test.txt
    (myenv) $ python runtests.py

Credits
---------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage
