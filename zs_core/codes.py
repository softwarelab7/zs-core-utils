from django.utils.translation import ugettext as _

AUTHORIZED = 'Authorized'
SETTLED = 'Settled'
PENDING = 'Pending'
REFUND = 'Refund'
DEBIT = 'Debit'
CREDIT = 'Credit'
TRANSFERRED = 'Transferred'
TRANSFER_PAID = 'Transfer Paid'
TRANSFER_FAILED = 'Transfer Failed'

# uses authnet codes
SOURCE_TYPE_MAP = {
    'authorized': ('Stripe', 'CC', 'amount_allocated', AUTHORIZED),
    'settled': ('Stripe', 'CC', 'amount_debited', SETTLED),
    'refund': ('Stripe', 'CC', 'amount_refunded', REFUND),
    'debit': ('Direct', 'Cash', 'amount_debited', DEBIT),
    'credit': ('Direct', 'Cash', 'amount_refunded', CREDIT),
    'transferred': ('Stripe', 'CC', 'amount_allocated', TRANSFERRED),
    'transfer-paid': ('Stripe', 'CC', 'amount_allocated', TRANSFER_PAID),
    'transfer-failed': ('Stripe', 'CC', 'amount_allocated', TRANSFER_FAILED)
}

PENDING = 'Pending'
PROCESSED = 'Processed'
CONFIRMED = 'Confirmed'
CANCELLED = 'Cancelled'
RETURNED = 'Returned'
PARTIAL_RETURN = 'Partial Return'
NEW = 'New'
FINISHED = 'Finished'

QTY_DEFAULT = 1

# status can be NEW, FINISHED, or CANCELLED
STATUS_CHOICES = (
    (NEW, _("New")),
    (FINISHED, _("Finished")),
    (CANCELLED, _("Cancelled")),
)

UNAVAILABLE = 'unavailable'
BOOKING = 'booking'
PAID = 'Paid'
FOR_APPROVAL = 'For Approval'

STYLIST = 'stylist'
REGULAR = 'regular'
IS_STYLIST_MAP = {
    STYLIST: (True, _("All stylists")),
    REGULAR: (False, _("All regular users"))
}

YES = 'yes'
NO = 'no'
IS_REVIEWED_MAP = {
    YES: (True, _("reviewed")),
    NO: (False, _("not yet reviewed")),
}

BOOKABLE = 'Bookable'

CHARGE = 'charge'
TRANSFER = 'transfer'

STRIPE_OBJECT_CHOICES = (
    (CHARGE, _("Charge")),
    (TRANSFER, _("Transfer")),
)

UPCOMING = 'Upcoming'
ENDED = 'Ended'
