class AppointmentPhoneNumberMixin(object):
    def get_phone_number(self, obj):
        if obj.order and obj.order.user.phone_number:
            return str(obj.order.user.phone_number)
        elif obj.address and obj.address.phone_number:
            return str(obj.address.phone_number)
        else:
            return ""
