from django import forms
from django.forms.utils import flatatt, to_current_timezone
from django.forms.widgets import DateInput, MultiWidget
from django.utils.encoding import force_unicode
from django.utils.safestring import mark_safe


class BootstrapTimePickerWidget(forms.TimeInput):
    def render(self, name, value, attrs=None):
        if value is None:
            value = ''
        final_attrs = self.build_attrs(attrs, type=self.input_type, name=name)
        final_attrs['class'] = 'timepick input-small'
        if value != '':
            # Only add the 'value' attribute if a value is non-empty.
            final_attrs['value'] = force_unicode(self._format_value(value))
        return mark_safe(u'<div class="input-append bootstrap-timepicker">\
            <input{} /><span class="add-on"><i class="icon-time"></i></span>\
            </div>'.format(flatatt(final_attrs)))


class CustomSplitDateTimeWidget(MultiWidget):
    """
    Custom version of the SplitDateTimeWidget
    """
    supports_microseconds = False

    def __init__(self, attrs=None, date_format=None, time_format=None):
        widgets = (DateInput(attrs=attrs, format=date_format),
                   BootstrapTimePickerWidget(attrs=attrs, format='%H:%M:%S'))
        super(CustomSplitDateTimeWidget, self).__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            value = to_current_timezone(value)
            return [value.date(), value.time().replace(microsecond=0)]
        return [None, None]
